
# 2018 Aug. 14.
# Ryuichi Hashimoto

# convert \" to ""
# synopsis: COMMAND < FILE1 > FILE2

# ruby2.5

DIRSEPARATOR = File::ALT_SEPARATOR || File::SEPARATOR

# get script-name
script_name = File.basename( __FILE__ )

# check number of command-line-parameters
if ( ARGV.size() != 0 ) then
  STDERR.puts "Invalid number of command-line-arguements"      
  STDERR.puts "synopsys: COMMAND < FILE1 > FILE2"
  exit(1)
end
         
# read standard-in
# output to standard-out
while line = gets
  # convert \" to ""
  line = line.chomp.gsub(/(?<!\\)\\"/,'""')
  puts line
end
exit 0

