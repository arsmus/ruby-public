#
# 2018 Aug. 07.
# 2018 Aug. 06.
# 2018 Aug. 05.
# Ryuichi Hashimoto.

# Output count of columns of the csv-file.

# Synopsis: COMMAND < CSV-FILE

# get script-name
script_name = File.basename( __FILE__ )

# check the number of command-line-arguements
if ( ARGV.size() != 0 ) then
  STDERR.puts "Invalid number of command-line-arguements"      
  STDERR.puts "synopsys: #{script_name} < CSV-FILE"
  exit(1)
end

column_info=`ruby csv-count_columns_each_line.rb`
# column_infoの1行目は"columns rows"というヘッダ
# 2行目以降がデータ
# 全ての行のカラム数が同じなら2行目のデータがあるだけ。
# 行によってカラム数が異なれば、column_infoに3行目以降が存在する
# データは、ヘッダに示すように、
# カラム数とそのカラム数を持つ行数を空白文字で区切っている
count_lines = column_info.count("\n")
if (2 != count_lines) then
  STDERR.puts "Error: Numbers of each columns are not same."
  exit(1)
end
print column_info.split("\n")[1].split(" ")[0],"\n"

