
# 2018 Aug. 10.
# 2018 Aug. 09.
# 2018 Aug. 08.
# Ryuichi Hashimoto.


require 'csv'

DIRSEPARATOR = File::ALT_SEPARATOR || File::SEPARATOR

# get script-name
@script_name = File.basename( __FILE__ )

class CSVCountColumns

  # output number of columns of each line in a csv-file.
  # 添字を行番号(先頭行が0)とするカラム数の配列を返すメソッド
  # input: STDIN
  def self.count_columns_each_line

    # 添字を行番号(先頭行が0)とするカラム数の配列
    columns = []

    # read CSV-file through STDIN
    while line = gets
      # convert \" to ""
      line = line.chomp.gsub(/(?<!\\)\\"/,'""')

      # parse CSV
      #   quote_char => '"' としていても、
      #   ruby csvはダブルクォートされていない文字列を正しく読み込む
      CSV.parse(line, {skip_blanks: true, headers: false, converters: :numeric, encoding: "UTF-8", col_sep:",", quote_char: '"'} ) do |row|
        columns << row.size()
      end
    end
    return columns
  end # end of def


  # カラム数が同じの行の数のarray[[カラム数,行数]]にする
  # return ARRAY[[COLUMNS,rows]] by descendent order
  # input: STDIN
  def self.lines_of_columns
    columns = self.count_columns_each_line
    
    # 行ナンバー順に入っているカラム数の配列columnsを
    # カラム数が同じの行の数のハッシュ{カラム数=>行数}にする
    hash_columnnum_rows = Hash.new(0)
    columns.each do |column_number|
       hash_columnnum_rows[column_number] += 1
    end

    # カラム数の出現順の配列の作成
    #   hash_columnnum_rowsをハッシュの値(行数)でdescendingソート
    array_columnnum_rows = hash_columnnum_rows.sort_by{ |k, v| -v }
    return array_columnnum_rows
  end # def
end # class

