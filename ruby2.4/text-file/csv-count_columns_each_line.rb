
# 2018 Aug. 05.
# 2018 Aug. 05.
# Ryuichi Hashimoto.

# output number of columns of each line in a csv-file.

# synopsys: COMMAND CSV-FILE

# ruby2.4

require 'csv'

DIRSEPARATOR = File::ALT_SEPARATOR || File::SEPARATOR

# get script-name
script_name = File.basename( __FILE__ )

# check number of command-line-parameters
if ( ARGV.size() != 0 ) then
   STDERR.puts "Invalid number of command-line-arguements"      
   STDERR.puts "synopsys: COMMAND < CSV-FILE"
   exit(1)
end

# 添字を行番号(先頭行が0)とするカラム数の配列
columns = []

# read CSV-file
while line = gets
  # convert \" to ""
  line = line.chomp.gsub(/(?<!\\)\\"/,'""')

  # parse CSV
  #   quote_char => '"' としていても、
  #   ruby csvはダブルクォートされていない文字列を正しく読み込む
  CSV.parse(line, {skip_blanks: true, headers: false, converters: :numeric, encoding: "UTF-8", col_sep:",", quote_char: '"'} ) do |row|
    columns << row.size()
  end
end

# 行ナンバー順に入っているカラム数の配列columnsを
# カラム数が同じの行の数のハッシュ{カラム数=>行数}にする
hash_columnnum_rows = Hash.new(0)
columns.each do |column_number|
   hash_columnnum_rows[column_number] += 1
end

# カラム数の出現順の配列の作成
#   hash_columnnum_rowsをハッシュの値(行数)で昇順ソート
array_columnnum_rows = hash_columnnum_rows.sort_by{ |_, v| v }

# hash_columnnum_rowsを出力する
puts "columns rows"
array_columnnum_rows.each do |col_num, rows|
   puts "#{col_num} #{rows}"
end

exit(0)

